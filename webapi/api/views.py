from django.shortcuts import render
import json
import ast
from django.http import HttpResponse
import requests
# Create your views here.


# rs = requests.get('https://search.cdc.gov/srch/internet_clia/select?q=FACILITY:(*a*)&rows=50&distrib=false&sort=field(STATE_CD)%20asc,field(CITY_NAME)%20asc,field(PRVDR_NUM)%20asc&json.wrf=jQuery191008376396231339767_1610606451296&q=FACILITY%3A(*a*)&wt=json&rows=50&distrib=false&fl=PRVDR_NUM%2CFACILITY%2CCITY_NAME%2CZIP_CD%2CSTATE_CD%2CGNRL_FAC_TYPE_DESC%2CTYPE%2CPHNE_NUM%2CADDRESS%2CADDRESS2&_=1610606451298')
# response = rs.text.split('"docs":')[1].split("}})")[0]
# rss = json.loads(response)
# # rsss = ast.literal_eval(rss['PRVDR_NUM])
# print(rss[1]['PRVDR_NUM'][0])

def api(request):
    rs = requests.get('https://search.cdc.gov/srch/internet_clia/select?q=FACILITY:(*a*)&rows=50&distrib=false&sort=field(STATE_CD)%20asc,field(CITY_NAME)%20asc,field(PRVDR_NUM)%20asc&json.wrf=jQuery191008376396231339767_1610606451296&q=FACILITY%3A(*a*)&wt=json&rows=50&distrib=false&fl=PRVDR_NUM%2CFACILITY%2CCITY_NAME%2CZIP_CD%2CSTATE_CD%2CGNRL_FAC_TYPE_DESC%2CTYPE%2CPHNE_NUM%2CADDRESS%2CADDRESS2&_=1610606451298')
    response = rs.text.split('"docs":')[1].split("}})")[0]
    rss = json.loads(response)
    return render(request, "index.html", {'data' : rss})
